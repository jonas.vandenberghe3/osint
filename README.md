# Osint

This is a tool that automates OSINT by combining Spiderfoot with an extended version of CrossLinked. 
This version includes multiple tools to gather information about employees of a company. 
It also uses a custom-made Tor proxy to prevent IP-blocking.

## Use

example email_address_formats: {first}.{last}@company.com, {f}.{last}@company.com

```
git clone https://gitlab.com/ikdoeict/jonas.vandenberghe3/osint.git
cd osint
pip3 install -r requirements.txt
sudo python ./osint.py -n COMPANY_NAME -d COMPANY_DOMAIN -f EMAILFORMAT

```
> :warning: **When the Spiderfoot CLI is loaded make sure to use 'export <sid> -t json -f spiderfoot_output.json' to export your results before you exit the scan!**