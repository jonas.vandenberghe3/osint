# TorProxy

## Setup

```
git clone https://gitlab.com/ikdoeict/jonas.vandenberghe3/torproxy.git
cd ./torproxy
Make sure docker is installed
docker build -t torproxy .
docker run -d -p 9050:9050 --name tor-container torproxy
```

### Setup - Proxychains

```
sudo apt-get install proxychains
sudo nano /etc/proxychains.conf
Add: socks5 'ContainerIP' 9050 And Delete: socks4 127.0.0.1 9050
Check if it's working: proxychains curl https://check.torproject.org
```

### Setup - ProxyFoxy
Download the extension.

Add the configuration.

![FoxyProxy configuration](foxyproxy.png)

## Use - Proxychains
Add "proxychains" before your normal command. ex. proxychains curl https://example.com

## Use - ProxyFoxy

Turn on FoxyProxy and start browsing!

![FoxyProxy Turn on](foxyproxyOn.png)