import subprocess
import sqlite3
import csv
import pandas as pandas
import os
import time
import json
import argparse

#MISC
def banner():
    print('''
             ___    ___   ___   _  _   _____ 
            / _ \  / __| |_ _| | \| | |_   _|
           | (_) | \__ \  | |  | .` |   | |  
            \___/  |___/ |___| |_|\_|   |_|  
          
          ''')
    
def install_docker():
    try:
        subprocess.run(["sudo","docker", "--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except FileNotFoundError:
        print("Docker is not installed. Installing...")
        subprocess.run(["sudo", "apt-get", "update"])
        subprocess.run(["sudo", "apt-get", "install", "-y", "docker.io"])

def container_running(container_name):
    try:
        output = subprocess.check_output(["sudo", "docker", "ps", "--format", "{{.Names}}"]).decode('utf-8')
        if container_name in output.split('\n'):
                print("Container already running...")
                return True
        else:
            output = subprocess.check_output(["sudo", "docker", "ps", "-a"]).decode('utf-8')
            if container_name in output:
                subprocess.run(["sudo", "docker", "stop", container_name], check=True)  
                subprocess.run(["sudo", "docker", "rm", container_name], check=True)  
            return False
    except Exception as e:
        print(f"Failed to check container status: {e}")

def stop_container(container_names): 
    for container_name in container_names:
        try:
            print(f"Stopping and removing container {container_name}...")
            subprocess.run(["sudo", "docker", "stop", container_name], check=True)  
            subprocess.run(["sudo", "docker", "rm", container_name], check=True)  
        except Exception as e:
            print(f"Failed to Stop and remove container {container_name}: {e}")

def create_results_folder(company_name):
    if not os.path.exists(f"{company_name}_osint"):
        print("Creating results directory...")
        try:
            subprocess.run(["sudo", "mkdir", f"{company_name}_osint"])
            print("Creation successful")
        except Exception as e:
            print(f"Creation failed: {e}")
    else:
        print(f"Directory {company_name}_osint already exists")

def reset_files(files):
    for file in files:
        if os.path.exists(file):
            os.remove(file)

#TOR PROXY
def install_proxychains():
    try:
        subprocess.run(["proxychains", "--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except FileNotFoundError:
        print("ProxyChains is not installed. Installing...")
        subprocess.run(["sudo", "apt-get", "install", "-y", "proxychains"])

def configure_proxychains():
    print("Configuring ProxyChains...")
    try:
        with open("/etc/proxychains.conf", "r") as config_file:
            lines = config_file.readlines()
            if "socks5 172.17.0.2 9050\n" not in lines:
                with open("/etc/proxychains.conf", "a") as config_file:
                    config_file.write("socks5 172.17.0.2 9050\n")
                    print("Tor proxy added to ProxyChains configuration successfully")
    except Exception as e:
        print(f"Failed to configure ProxyChains: {e}")

def start_tor_proxy(tor_proxy_container_name):
    print("Starting Tor-proxy...")
    if not container_running(tor_proxy_container_name):
        try: 
            subprocess.run(["sudo", "docker", "run", "-d", "-p", "9050:9050", "--name", tor_proxy_container_name, "torproxy"])
        except Exception as e:
            print(f"Failed to start Tor proxy: {e}")

#SPIDERFOOT
def install_spiderfoot():
    if not os.path.exists("spiderfoot"):
        print("Installing Spiderfoot...")
        try:
            subprocess.run(["git", "clone", "https://github.com/smicallef/spiderfoot.git"])
            subprocess.run(["pip3", "install", "-r",  "spiderfoot/requirements.txt"])
            print("SpiderFoot installed successfully")
        except Exception as e:
            print(f"SpiderFoot installation failed: {e}")
    else:
        print("SpiderFoot is already installed")

def start_spiderfoot(company_name):
    try:
        subprocess.Popen(["sudo", "python3", "spiderfoot/sf.py", "-l", "127.0.0.1:5001"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        time.sleep(15)
        start_spiderfoot_scan(company_name)
    except Exception as e:
        print(f"Failed to start Spiderfoot: {e}")

def start_spiderfoot_scan(company_name):
    try:
        print("Starting Spiderfoot CLI, don't forget to use 'export <sid> -t json -f spiderfoot_output.json' to export your results before exiting...")
        print("Use exit when done to exit Spiderfoot and continue")
        subprocess.run(["sudo", "python3", "spiderfoot/sfcli.py"])
        get_spiderfoot_results(company_name)
    except Exception as e:
        print(f"Failed to scan with Spiderfoot: {e}")

def get_spiderfoot_results(company_name):
    try:
        with open('spiderfoot_output.json', 'r') as file:
            data = json.load(file)
        
        extracted_data = []
        
        for entry in data:
            extracted_data.append({
                "data": entry.get("data"),
                "module": entry.get("module")
            })

        df = pandas.DataFrame(extracted_data)

        with pandas.ExcelWriter("spiderfoot_data.xlsx") as writer:
            for module, module_df in df.groupby("module", as_index=False):
                module_df.drop(columns="module", inplace=True, errors="ignore")
                module_df.to_excel(writer, sheet_name=str(module), index=False)

        print(f"Results saved to {company_name}_osint/technical_output.xlsx")

    except Exception as e:
        print(f"Failed to get Spiderfoot results: {e}")

#DORXNG
def start_dorxng_container(dorxng_container_name):
    if not container_running(dorxng_container_name):
        try: 
            subprocess.run(["sudo", "docker", "run", "-d", "--name", dorxng_container_name, "researchanddestroy/searxng:latest"])
            print("Starting the container please wait...")
            time.sleep(45)
            print("DorXNG Container started successfully.")
        except Exception as e:
            print(f"Failed to start DorXNG Container: {e}")       

def start_dorxng(dorks, dorxng_container_name, company_name):
    dorxng_files=["dorxng.db","dork_output.csv",f"{company_name}_osint/dork_output.xlsx"]
    try:
        start_dorxng_container(dorxng_container_name)
        reset_files(dorxng_files)
        for dork in dorks:
            print(f"Starting Dork: {dork}...")
            subprocess.run(["sudo", "python", "DorXNG/DorXNG.py", "-c", "4", "-n", "64", "-q", dork])
            print(f"Adding results to dork_output.csv file...")
            save_dork_results_csv(dork)
        save_dorks_results_xlsx(f"{company_name}_osint/dork_output.xlsx")
    except Exception as e:
        print(f"Failed to start DorXNG: {e}")

def save_dork_results_csv(dork):
    connection = sqlite3.connect('dorxng.db')
    cursor = connection.cursor()
    try:
        cursor.execute(f"SELECT url FROM search_results WHERE query='{dork}' LIMIT 50")
        rows = cursor.fetchall()

        with open('dork_output.csv', 'a', newline='', encoding='utf-8') as csv_file:
            csv_writer = csv.writer(csv_file)

            if csv_file.tell() == 0:
                csv_writer.writerow(['Dork', 'Result'])

            for row in rows:
                csv_writer.writerow([dork, row[0]])

        print("Results saved to dork_output.csv")

    except Exception as e:
        print(f"Failed to save Dorks to dork_output.csv: {e}")

    finally:
        connection.close()

def save_dorks_results_xlsx(directory):
    try:
        df = pandas.read_csv('dork_output.csv')

        df.to_excel(directory, index=False, header=['Dork', 'Result'])

        print(f"Results saved to {directory}")

    except Exception as e:
        print(f"Failed to save Dorks to {directory}: {e}")

#CROSSLINKED
def start_crosslinked(company_name, email_format):
    try:
        subprocess.run(["sudo","python", "CrossLinkedExtended/crosslinkedextended.py", "-f", email_format, company_name])
    except Exception as e:
        print(f"Failed to start CrossLinked: {e}")

def main(company_name, company_domain, email_format):

    tor_proxy_container_name = "tor-container"
    dorxng_container_name = "dorxng_container"
    container_names = [tor_proxy_container_name, dorxng_container_name]
    dorks = [f'filetype:pdf site:{company_domain}', f'intitle:"index of" site:{company_domain}', f'inurl:login site:{company_domain}' f'site:{company_domain} intext:(jobs | job | vacatures | vacature)']
    
    install_docker()

    install_proxychains()

    configure_proxychains()

    start_tor_proxy(tor_proxy_container_name)

    install_spiderfoot()

    create_results_folder(company_name)

    banner()
    
    print(f"Starting OSINT-investigation for: {company_name}")

    start_spiderfoot(company_name)

    start_crosslinked(company_name, email_format)
    
    start_dorxng(dorks, dorxng_container_name, company_name)

    stop_container(container_names)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process company information.")
    parser.add_argument("-n", "--company_name", required=True, help="Name of the company")
    parser.add_argument("-d", "--company_domain", required=True, help="Domain of the company")
    parser.add_argument("-f", "--emailformat", required=True, help="Email address format")

    args = parser.parse_args()

    main(args.company_name, args.company_domain, args.emailformat)